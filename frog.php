<?php

require_once('animal.php');

class frog extends animal {
    public $name;
    public $legs = 4;
    public $cold_blooded = "No";
    public function jump() {
        echo "Jump : Hop Hop";
    }
}