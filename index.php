<?php

require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$sheep = new animal("shaun");

echo "Name : $sheep->name <br>";
echo "Legs : $sheep->legs <br>";
echo "Cold Blooded : $sheep->cold_blooded <br> <br>";

$sunggokong = new ape("kera sakti");

echo "Name : $sunggokong->name <br>";
echo "Legs : $sunggokong->legs <br>";
echo "Cold Blooded : $sunggokong->cold_blooded <br>";
$sunggokong->yell();

$kodok = new frog("buduk");

echo "Name : $kodok->name <br>";
echo "Legs : $kodok->legs <br>";
echo "Cold Blooded : $kodok->cold_blooded <br>";
$kodok->jump();

